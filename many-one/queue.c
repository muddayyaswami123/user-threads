#include "queue.h"

void init_queue(queue *queue)
{
    queue->front = NULL;
    queue->rear = NULL;
    queue->nodesCount = 0;
};

void enqueue(queue *queue, thread *thread)
{
    node *newthreadNode = (node *)malloc(sizeof(node));
    if (!newthreadNode)
    {
        perror("Error: Unable to allocate memory");
        return;
    }
    newthreadNode->thread = thread;
    if (!queue->nodesCount || (!queue->front && !queue->rear))
    {
        queue->front = queue->rear = newthreadNode;
    }
    else
    {
        queue->rear->nextNode = newthreadNode;
        newthreadNode->prevNode = queue->rear;
        queue->rear = newthreadNode;
    }
    queue->nodesCount++;
    return;
};

thread *dequeue(queue *queue)
{
    if (!queue->nodesCount || (!queue->front && !queue->rear))
        return NULL;
    thread *removedThread = queue->front->thread;
    if (queue->nodesCount == 1 || (queue->front == queue->rear))
        queue->front = queue->rear = NULL;
    else
    {
        queue->front = queue->front->nextNode;
        queue->front->prevNode = NULL;
    }
    queue->nodesCount--;
    return removedThread;
}

void traverseQueue(queue *queue)
{
    if (!queue->nodesCount || (!queue->front && !queue->rear))
        printf("Queue is empty\n");
    else
    {
        node *tempNode = queue->front;
        while (tempNode)
        {
            //printf("%u", *tempNode->thread->tid);
            tempNode = tempNode->nextNode;
        }
    }
    return;
}

thread *searchThread(queue *queue, thread_t tid)
{
    if (!queue->nodesCount || (!queue->front && !queue->rear))
        return NULL;
    else
    {
        node *tempNode = queue->front;
        while (tempNode)
        {
            if (tempNode->thread->tid == tid)
                return tempNode->thread;
            tempNode = tempNode->nextNode;
        }
    }
    return NULL;
}

// int main()
// {
//     queue q;
//     int a = 5, b = 4;
//     init_queue(&q);
//     enqueue(&q, &a);
//     enqueue(&q, &b);
//     traverseQueue(&q);
//     dequeue(&q);
//     dequeue(&q);
//     traverseQueue(&q);
//     return 0;
// }
