#include "thread.h"

typedef struct linkedListNode
{
    thread *thread;
    struct linkedListNode *nextNode;
    struct linkedListNode *prevNode;
} node;

typedef struct queue
{
    node *front;
    node *rear;
    int nodesCount;
} queue;

void init_queue(queue *q);
void enqueue(queue *q, thread *thread);
thread *dequeue(queue *q);
void traverseQueue(queue *queue);
thread *searchThread(queue *queue , thread_t tid);
