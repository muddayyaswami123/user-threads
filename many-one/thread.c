#include "queue.h"

// Timer Basic Setup

itimerval timer;
#define STOP_TIMER 0
#define START_TIMER 1
#define ALARM_INTERVAL 2000 // 4msec

queue *threadQueue;
thread *currentThread;
thread_t threadCount = 0;

void initQueue(void)
{
    threadQueue = (queue *)malloc(sizeof(queue));
    init_queue(threadQueue);
    return;
}

void initCurrentThread(void)
{
    currentThread = (thread *)malloc(sizeof(thread));
    currentThread->tid = threadCount++;
    currentThread->threadStatus = RUNNING;
    currentThread->threadJoinState = JOINABLE;
    currentThread->routine = NULL;
    currentThread->args = NULL;
    currentThread->retval = NULL;
    currentThread->signalType = -1;
    sigsetjmp(currentThread->env, 1);
    return;
}

// https://www.youtube.com/watch?v=_1TuZUbCnX0&ab_channel=IndronilBanerjee
void initTimerSignal()
{
    struct sigaction action;
    action.sa_handler = &scheduler;
    // sigset_t : array of booleans representing a set of signals; action.sa_mask -> sigset_t
    sigemptyset(&action.sa_mask);          // initializes all bits by clearing those
    sigaddset(&action.sa_mask, SIGVTALRM); // sets bit corresponding to SIGVTALARM
    action.sa_flags = 0;
    sigaction(SIGVTALRM, &action, NULL);
    atexit(cleanThreadQueue); // Registering cleanThreadQueue function which will run at the end of program
    timerFunction(&timer, START_TIMER);
    return;
};

void init(void)
{
    initQueue();
    initCurrentThread();
    initTimerSignal();
    return;
};

void cleanThreadQueue()
{
    for (int i = 0; i < threadQueue->nodesCount; i++)
        free(dequeue(threadQueue));
    free(threadQueue);
};

void funcStartRoutine(void)
{
    currentThread->retval = currentThread->routine(currentThread->args);
    currentThread->threadStatus = TERMINATED;
    raise(SIGVTALRM);
    return;
};

void timerFunction(itimerval *timer, unsigned int timerAction)
{
    timer->it_value.tv_sec = 0;
    timer->it_value.tv_usec = timerAction ? ALARM_INTERVAL : 0;
    timer->it_interval.tv_sec = 0;
    timer->it_interval.tv_usec = timerAction ? ALARM_INTERVAL : 0;
    setitimer(ITIMER_VIRTUAL, timer, 0);
}

void scheduler(int signalType)
{
    timerFunction(&timer, STOP_TIMER);
    // The setjmp() function saves various information about the calling
    //    environment (typically, the stack pointer, the instruction
    //    pointer, possibly the values of other registers and the signal
    //    mask) in the buffer env for later use by longjmp().  In this
    //    case, setjmp() returns 0.

    //    The longjmp() function uses the information saved in env to
    //    transfer control back to the point where setjmp() was called and
    //    to restore ("rewind") the stack to its state at the time of the
    //    setjmp() call.
    thread_t tempThreadTID = currentThread->tid;
    if (sigsetjmp(currentThread->env, 1))
        return;
    // Pushing current thread to queue
    enqueue(threadQueue, currentThread);
    // RUNNING to READY -- Keeping this thread on hold and looking for next thread to give chance to execute
    if (currentThread->threadStatus == RUNNING)
        currentThread->threadStatus = READY;
    thread *tempThread;
    for (int i = 0; i < threadQueue->nodesCount; i++)
    {
        tempThread = dequeue(threadQueue);
        if (tempThread->threadStatus == READY)
        {
            currentThread = tempThread;
            currentThread->threadStatus = RUNNING;
            break;
        }
        else if (tempThread->threadStatus == TERMINATED)
            enqueue(threadQueue, tempThread);
    }

    if (currentThread->tid == tempThreadTID)
        exit(0);
    if (currentThread->signalType != -1)
    {
        raise(currentThread->signalType);
        currentThread->signalType = -1;
    }
    timerFunction(&timer, START_TIMER);
    siglongjmp(currentThread->env, 1);
    return;
}

int create_thread(thread_t *threadId, void *(*routine)(void *), void *args)
{
    thread *tempThread = (thread *)malloc(sizeof(thread));
    tempThread->tid = threadCount++;
    tempThread->args = args;
    tempThread->threadStatus = READY;
    tempThread->routine = routine;
    tempThread->signalType = -1;
    tempThread->threadJoinState = JOINABLE;
    tempThread->stack = mmap(NULL, STACK_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    *threadId = tempThread->tid;
    sigsetjmp(tempThread->env, 1);
    tempThread->env[0].__jmpbuf[6] = i64_ptr_mangle((long int)tempThread->stack + STACK_SIZE - sizeof(long int));
    tempThread->env[0].__jmpbuf[1] = i64_ptr_mangle((long int)tempThread->stack + STACK_SIZE - sizeof(long int));
    tempThread->env[0].__jmpbuf[7] = i64_ptr_mangle((long int)funcStartRoutine);
    enqueue(threadQueue, tempThread);
    return 0;
}

int myThread_join(thread_t tid, void **retval)
{
    if (tid == currentThread->tid)
        return EDEADLK;
    thread *tempThread = searchThread(threadQueue, tid);
    if (!tempThread)
        return ESRCH;
    tempThread->threadJoinState = JOINED;
    while (1)
    {
        if (tempThread->threadStatus == TERMINATED)
            break;
    }

    *retval = tempThread->retval;
    return 0;
};


void mythread_exit(void *retval){

    currentThread->retval = retval;
    currentThread->threadStatus = TERMINATED;
    //calling scheduler to run another process
    raise(SIGVTALRM);
}

int mythread_kill(thread_t thread_id, int signal){
    if(signal == 0) return 0;
    //check if proper signal number is present else return with proper error number
    if(signal <0 || signal > 64)    return EINVAL;
    //value to be returned by kill
    int killret = -1;   
    if(thread_id == currentThread->tid) {
        killret = raise(signal);
    }
    else{
        thread *temp_thread = searchThread(threadQueue, thread_id);
        if(temp_thread == NULL){
            killret = ESRCH;
        }
        else{
            temp_thread->signalType = signal;
            killret = 0;
        }
    }

    return killret;

}