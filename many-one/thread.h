#include <stdio.h>
#include <setjmp.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <errno.h>

#define STACK_SIZE 1024 * 1024
typedef unsigned long thread_t;
typedef struct itimerval itimerval;

// States for Thread -- Thread has JOINED or NOT to Main
enum threadJoinState
{
    JOINABLE,
    JOINED
};

// Thread could have following status -- READY (Waiting to get CPU), RUNNING(Running on CPU), TERMINATED(Terminated execution)
enum threadStatus
{
    READY,
    RUNNING,
    TERMINATED
};

typedef struct thread
{
    thread_t tid;
    enum threadJoinState threadJoinState;
    enum threadStatus threadStatus;
    char *stack;
    void *(*routine)(void *);
    void *args;
    void *retval;
    // transferring execution from one function to a predetermined location in another function -- https://man7.org/linux/man-pages/man3/setjmp.3.html
    jmp_buf env;
    int signalType;
} thread;

void initQueue(void);
void initCurrentThread(void);
void initTimerSignal();
void init(void);
void cleanThreadQueue();
void funcStartRoutine(void);
int create_thread(thread_t * tid, void *(*routine)(void *), void *args);
int myThread_join(thread_t tid, void **retval);
void timerFunction(itimerval *timer, unsigned int timerAction);
void scheduler(int signalType);
void mythread_exit(void *retval);
int mythread_kill(thread_t thread_id, int signal);

// Credit : https://sites.cs.ucsb.edu/~chris/teaching/cs170/projects/proj2.html
static long int i64_ptr_mangle(long int p)
{
    long int ret;
    asm(" mov %1, %%rax;\n"
        " xor %%fs:0x30, %%rax;"
        " rol $0x11, %%rax;"
        " mov %%rax, %0;"
        : "=r"(ret)
        : "r"(p)
        : "%rax");
    return ret;
}
