#ifndef THREAD_SPINLOCK
#define THREAD_SPINLOCK

typedef unsigned int thread_spinlock_t;

void mythread_spinlock_init(thread_spinlock_t *lock);
int mythread_prot_spin_lock(thread_spinlock_t *lock);
int mythread_prot_spin_unlock(thread_spinlock_t *lock);

#endif