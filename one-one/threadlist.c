#include <stdio.h>
#include <stdlib.h>
#include "threadlist.h"

void initThreadList(struct threadList *tl)
{
    tl->front = NULL;
    tl->rear = NULL;
    return;
}

void enqueue(struct threadList *tl, struct thread *td)
{
    struct threadUnit *tdu = malloc(sizeof(struct threadUnit));
    if (!tdu)
    {
        printf("Oops! Couldn't get memory ;(\n");
        exit(EXIT_FAILURE);
    }
    tdu->threadptr = td;
    // printf("%lu is thread id in enqueue with retval %d\n",td->t_id,*(int *)td->retval);
    tdu->next = NULL;
    if (!tl->front)
    {
        tl->front = tl->rear = tdu;
    }
    else
    {
        tl->rear->next = tdu;
        tl->rear = tdu;
    }
    return;
}

void dequeue(struct threadList *tl){
    if(!tl->front)  return;
    struct threadUnit *cur = tl->front->next;
    free(tl->front);
    tl->front = cur;
    return;
}
struct thread *getThreadFromQueue(struct threadList *tl, thread_t threadId){
    if(!tl->front)  return NULL;
    struct threadUnit * cur = tl->front;
    while(cur->threadptr->t_id != threadId){
        cur = cur->next;
    }
    if(cur){
        return cur->threadptr;
    }
    else{
        return NULL;
    }

}
