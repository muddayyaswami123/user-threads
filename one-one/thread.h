#ifndef STRUCT_THREAD_H
#define STRUCT_THREAD_H

#define CANJOIN 1
#define ISJOINED 2
#define ISSIGNAL 3
typedef unsigned long int thread_t;

typedef struct thread{
    thread_t t_id;
    int status;
    int p_id;
    void* stack;
    void*(*functptr) (void*);
    void* args;
    void* retval;
}thread;

void add_mainThread_to_list(thread_t mtd);
void clean_threadlist();
int create_thread(thread_t *t_id, void*(*functptr)(void *), void*arg);
int func_start_routine(void *args);
int myThread_join(thread_t tid, void **retval);
thread_t get_self_th_id();
void mythread_exit(void *retval);
int mythread_kill(thread_t thread_id, int signal);

#endif