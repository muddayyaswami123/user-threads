#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <signal.h>
#include "threadlist.h"

#define stack_size 1024*1024
#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); } while (0)

thread_t MainThread_id;
struct threadList tl;
struct threadList* Threadqueue = &tl;


void add_mainThread_to_list(thread_t mtd){
    struct thread * maintd;
    maintd = (thread *) malloc(sizeof(thread));
    maintd->t_id = mtd;
    maintd->status = CANJOIN;
    enqueue(Threadqueue, maintd);
    return;

}
void clean_threadlist(){
    // printf("clean is being called\n");
    struct  threadUnit* cur = Threadqueue->front;
    int c = 0;
    if(!cur)    return;
    while(cur!=NULL){
        c++;
        cur = cur->next;
    }
    for(int i=0;i<c;i++){
        dequeue(Threadqueue);
    }
    free(Threadqueue);
    return;   
}
int func_start_routine(void * t){
    thread *my_thread = (thread *) t;
    my_thread->retval = my_thread->functptr(my_thread->args);
    // printf("return in start routine %d\n",*(int *)my_thread->retval);
    return 0;
}


thread_t get_self_th_id(){
    thread_t Pid = gettid();
    return Pid;
}
int create_thread(thread_t *t_id, void*(*functptr) (void*), void*arg){
    
    if(!Threadqueue->front){
        initThreadList(Threadqueue);
        MainThread_id = get_self_th_id();
        add_mainThread_to_list(MainThread_id);
    }

    struct thread* my_thread;
    char* stack; // thread stack which will grow downnwards
    my_thread = (struct thread*) malloc(sizeof(struct thread));
    my_thread->functptr = functptr;
    my_thread->args = arg;
    
    long int thread_id;
    // allocating memory to stack using mmap system call

    stack = mmap(NULL,stack_size , PROT_READ | PROT_WRITE,MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    
    // exiting with error, if mmap does not allocate memory to stack
    if (stack == MAP_FAILED)
        errExit("mmap");

    my_thread->stack = (void*) stack;      // keeping reference of stack in our thread structure.
    my_thread->status = CANJOIN;
    
    //calling clone system call on given function pointer with thread stack and proper flags set along with arguments
    //printf("return value in thread before clone %d\n",*(int *) my_thread->retval);
    thread_id = clone(func_start_routine,my_thread->stack+ stack_size, SIGCHLD|CLONE_FS|CLONE_FILES|CLONE_VM,(void *)my_thread);
    
    // if clone is failed, then return with errno.
    if(thread_id == -1){
        return EINVAL;
    }
    // setting the thread id in given thread variable 
    *t_id = thread_id;

    my_thread->t_id = thread_id;  //setting t_id of my_thread 
    my_thread->p_id = getpid();
    // printf("\n%lu is thread child id\n", my_thread->t_id);
    enqueue(Threadqueue, my_thread);
    //printf("\nthread id of main in list%lu\n",Threadqueue->front->threadptr->t_id);
    // printf("return value in list %d\n",*(int *) my_thread->retval);
    return 0;  // returns 0 on successful creation of thread.
}




// implement join on the given thread
int myThread_join(thread_t tid, void **retval){

     //creating a integer variable to indicate status of thread termination
    int td_status;

    //get thread with given thread_id from the list
    struct thread* td = getThreadFromQueue(Threadqueue, tid);
    //get caller/parent id 
    thread_t caller_id = get_self_th_id();
    //if caller id and given thread id are same, then deadlock occurs
    if(caller_id == tid){
        return EDEADLK;
    }
     //if any thread is not present with the given thread id.
    if(td == NULL){
        return ESRCH;
    }
   
    //if thread is already joined then return with appropriate error no
    if(td->status == ISJOINED){
        return EINVAL;
    }
    //doing the actual join on thread if the status is canjoin
    if(td->status == CANJOIN){
        td->status = ISJOINED;
        waitpid(tid, &td_status,0);
        if(td_status == -1){
            errExit("mythread_join on thread failed due to waitpid error");
        }
    }
    // printf("retval in join%d\n",*(int *)td->retval);
        *retval = td->retval;
        // printf("I am here\n");
    // printf("retval in join is and %d \n", *(int *)td->retval);
    return 0; //return 0 on successfully joining a thread;
}


void mythread_exit(void *retval){
    thread_t ex_tid = get_self_th_id();
    // printf("this is ex_tid %lu\n",ex_tid);
    // printf("this is main id %lu\n",MainThread_id);
    //sleep(0.01);
    thread *ex_thread = getThreadFromQueue(Threadqueue, ex_tid);

    //if thread id is of main, then first terminate ie join all its child and then exit main
    if(ex_tid == MainThread_id){
        if(Threadqueue->front == NULL){
            errExit("queue is empty");
        }
       struct threadUnit * cur = Threadqueue->front->next;
        while(cur != NULL){
            if(cur->threadptr->status == CANJOIN){
                myThread_join(cur->threadptr->t_id, NULL);
            }
            cur = cur->next;
        }
        clean_threadlist();
        exit(0);
    }
    if(ex_thread == NULL){
        perror("No thread with given id found!!");
        return;
    }
    // printf("this retval in exit %d\n",*(int *)retval);
    ex_thread->retval = retval;
    exit(0);
}

int mythread_kill(thread_t thread_id, int signal){

    if(signal == 0) return 0;
    //check if proper signal number is present else return with proper error number
    if(signal <0 || signal > 64)    return EINVAL;

    thread * killing_thread = getThreadFromQueue(Threadqueue, thread_id);

    //if thread to be killed is not present return with proper error number.
    if(killing_thread == NULL)  return  ESRCH;

    //if thread is present then change it's status and kill the thread

    killing_thread->status = ISSIGNAL;
    int kill_status = kill(thread_id,signal);

    //return status of kill

    return kill_status;


}

