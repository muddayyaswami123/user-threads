#include <stdio.h>
#include "thread.h"
#include "thread_lock.h"


long c = 0, c1 = 0, c2 = 0,c3 = 0, run = 1;
thread_spinlock_t lock = 0;

void *thread1(void *arg) {
    while(run == 1) {
    mythread_prot_spin_lock(&lock);
        c++;
    mythread_prot_spin_unlock(&lock);    
        c1++;
    }
}


void *thread2(void *arg) {
    while(run == 1) {
     mythread_prot_spin_lock(&lock);
        c++;
    mythread_prot_spin_unlock(&lock);
        c2++;
    }
}


void *thread3(void *arg) {
    while(run == 1) {
     mythread_prot_spin_lock(&lock);
        c++;
    mythread_prot_spin_unlock(&lock);
        c3++;
    }
}


int main(){
    	printf("======================Race probelm testing========================\n");
    thread_t th1, th2,th3;
    create_thread(&th1, thread1,NULL);
    create_thread(&th2,thread2,NULL);
    create_thread(&th3,thread3,NULL);
    sleep(2);
    run = 0;
    printf("c = %ld c1+c2+c3 = %ld c1 = %ld c2 = %ld c3 = %ld \n", c, c1+c2+c3, c1, c2, c3);
    	printf("====================================================================\n");
}