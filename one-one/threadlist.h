#include "thread.h"

struct threadUnit
{
    struct thread *threadptr;
    struct threadUnit *next;
};

struct threadList
{
    struct threadUnit *front;
    struct threadUnit *rear;
};

void initThreadList(struct threadList *tl);
void enqueue(struct threadList *tl, struct thread *td);
void dequeue(struct threadList *tl);
struct thread *getThreadFromQueue(struct threadList *tl, thread_t threadId);