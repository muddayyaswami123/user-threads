#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "thread.h"

void *myThreadFun(void *vargp)
{
    int *temp = (int *) vargp;
    int temp2 = 500;
    int *tmp2 = &temp2;
    mythread_exit((void *)tmp2);
}
//joining on thread which does not exist;
void test3(){
    thread_t td =0;
    int t = 100;
    int *tp = &t;
    create_thread(&td,myThreadFun, (void *) tp);
    if(td != 0){
        printf("test3 PASS\n");
    }
    else{
        printf("test3 FAIL\n");
    }

}
// checks if join and exit works properly and return expected value 
void test2(){
    thread_t td =0;
    int * joinret = NULL;
    int t = 100;
    int *tp = &t;
    create_thread(&td,myThreadFun, (void *) tp);
    myThread_join(td, (void **)&joinret);

    if(*(int *)joinret == 500){
        printf("test2 PASS\n");
    }
    else{
        printf("test2 FAIL");
    }
    
}

//checking if create thread works
void test1(){
    thread_t td1;
    int * joinret = NULL;
    int status = 0;
    status = myThread_join(td1, (void **)&joinret);
    if(status == ESRCH){
        printf("test1 PASS with errno %d\n", status);
    }
    else{
        printf("test1 FAIL");
    }

}

int main(){
    printf("===========Function testing===============\n");
    test1();
    test2();
    test3();
    printf("====================function testing end===========\n");
}