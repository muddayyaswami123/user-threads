
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "thread_lock.h"


// The atomic function _sync_lock_test_and_set and _sync_lock_release 
// are referred from the standard gnu documentation: 
// https://gcc.gnu.org/onlinedocs/gcc-4.1.0/gcc/Atomic-Builtins.html

void mythread_spinlock_init(thread_spinlock_t *lock){
    *lock = 0;
    return;
}

int mythread_prot_spin_lock(thread_spinlock_t *lock){
    while(__sync_lock_test_and_set(lock,1)) ;
    return 0;
}

int mythread_prot_spin_unlock(thread_spinlock_t *lock){
    if(*lock == 1){
        __sync_lock_release(lock,0);
        return 0;
    }
    return EINVAL;

}

